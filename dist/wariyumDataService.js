'use strict';
// Source: app/js/app.js
var mod;

mod = angular.module('wairyum-service-mob', []);;;// Source: app/js/services/CategoryHelper.js
angular.module('wairyum-service-mob')
.service('CategoryHelper', function CategoryHelper($http, $q, $rootScope, $log, $resource, CONSTANTS) {
    
    this.getCategoryByParentId = function(parentId) {
    return $http({
            params: {
                parentId: parentId
            },
            method: 'GET',
          
            url: CONSTANTS.url + '/categoryHelper/getCategoryByParentId'

        });
    };
    this.getProductsByCategoryId = function(id) {

        return $http({
            params: {
                id: id 
            },
            method: 'GET',
         
            url: CONSTANTS.url + '/categoryHelper/getProductsByCategoryId'

        });
    };
});
;// Source: app/js/services/PurchaseHelper.js
angular.module('wairyum-service-mob')
    .service('PurchaseHitoryHelper', function CategoryHelper($http, $q, $rootScope, $log, $resource, CONSTANTS) {

        this.getPurchaseHistory = function(mobileDeviceId) {
            return $http({
                params: {
                    mobileDeviceId: mobileDeviceId
                },
                method: 'GET',

                url: CONSTANTS.url + '/purchaseHelper/getPurchaseHistory'

            });
        };
    });
;// Source: app/js/services/SearchHelper.js
angular.module('wairyum-service-mob')
    .service('SearchHelper', function SearchHelper($http, $q, $rootScope, $log, CONSTANTS) {
        this.getSearchResult = function(mobileDeviceId) {
          return $http({
                params: {
                    mobileDeviceId: mobileDeviceId
                },
                method: 'POST',
                cache: true,
                url: CONSTANTS.url + '/SearchHelper/getSearchResult'

            });
        };

        this.setSearch = function(searchCondition) {
            $log.debug('searchConditions:',searchCondition);
            return $http({
                method: 'POST',
                url: CONSTANTS.url + '/SearchHelper/setSearch',
                data: {
                    isOnCategory:searchCondition.category,
                    isOnProduct:searchCondition.product,
                    searchText:searchCondition.searchtext,
                    mobileDeviceId:CONSTANTS.mobileAppId
                }
            });
        };


    });
;// Source: app/js/services/mobileAppService.js
angular.module('wairyum-service-mob')
    .service('MobileAppService', function($log, CONSTANTS, $resource) {
        var Service = $resource(CONSTANTS.url + '/mobileApps/:id', {}, {
            query: {
                method: 'GET',
                params: {
                    offset: 0,
                    max: 10
                },
                isArray: true
                // headers: {
                //     'X-Auth-Token': $rootScope.authHeader
                // }

            }
        });
        this.get = function(offset, max, name) {
            // if ($rootScope.isAuthenticated) {
            return Service.query({
                offset: offset,
                name: name,
                max: max
            }, function(users) {

                // loaded = loaded + 3;
                //todo: limit to maximum products
                return users;
            });
            //};
        };
        this.save = function(mobileapp) {
            alert('in MobileAppService');
            alert(mobileapp.gcmId)
            return Service.save({}, mobileapp, function(response) {
                // Handle a successful response
                alert(response);
            }, function(response) {
                alert('error');
                // Handle a non-successful response
            });
        };
    });
;// Source: app/js/services/orderedlist.js
angular.module('wairyum-service-mob')
    .service('Orderedlist', function($log, CONSTANTS, $resource, $http) {
        this.getPurchaseOrders= function(purchaseId) {
            return $http({
                params: {
                    purchaseId:purchaseId
                },
                method: 'GET',
                url: CONSTANTS.url + '/purchaseHelper/getPurchaseOrders'

            });
        };
        this.getOrderedItems = function(mobileAppId) {
            return $http({
                params: {
                    mobileDeviceId: mobileAppId
                },
                method: 'GET',
                url: CONSTANTS.url + '/purchaseHelper/getOrderedItems'

            });
        };
        this.getOrderDetail = function(skuId) {
            return $http({
                params: {
                    mobileAppId: CONSTANTS.mobileAppId,
                    skuId: skuId
                },
                method: 'POST',
                url: CONSTANTS.url + '/purchaseHelper/getOrderDetail'

            });
        };
        this.saveorderitem = function(orderitem) {
            $log.debug('orderitem before save:', orderitem);
            return $http({
                params: {
                    mobileAppId: CONSTANTS.mobileAppId
                },
                method: 'POST',
                url: CONSTANTS.url + '/purchaseHelper/saveOrderItem',
                data: {
                    skuId: orderitem.skuId,
                    //id: orderitem.id,
                    quantity: orderitem.quantity
                }
            });
        };
        this.deleteOrderItem = function(orderitemId) {
            $log.debug('id of orderItem to be deleted', orderitemId);

            return $http({
                params: {
                    id: orderitemId
                },
                method: 'POST',
                url: CONSTANTS.url + '/purchaseHelper/deleteOrderItem'

            });
        };
        this.convertOrderToPurchase = function(mobileAppId) {
            return $http({
                params: {
                    mobileAppId: mobileAppId
                },
                method: 'POST',
                url: CONSTANTS.url + '/purchaseHelper/convertOrderToPurchase'

            });
        };


    });
;// Source: app/js/services/publicservice.js
angular.module('wairyum-service-mob')
    .service('Publicservice', function Publicservice($http, $q, $rootScope, $log, CONSTANTS, MobileAppService) {
        this.getProducts = function(offset, max) {

            return $http({
                method: 'GET',
                params: {
                    offset: offset,
                    max: max
                },
                cache: true,
                url: CONSTANTS.url + '/public/getProducts'
            });
        };
        this.getProductById = function(id) {
            return $http({
                params: {
                    id: id
                },
                method: 'GET',
                cache: true,
                url: CONSTANTS.url + '/public/getProductById'
            });
        };
        this.getMobileAppByAppId = function(appId) {
            return $http({
                params: {
                    appId: appId
                },
                method: 'GET',
                cache: true,
                url: CONSTANTS.url + '/public/getMobileAppByAppId'
            });
        };
        this.getOutlets = function() {
            return $http({
                method: 'GET',
                cache: true,
                url: CONSTANTS.url + '/public/getOutlets'

            });
        };
        this.getOutletsById = function(id) {

            return $http({
                params: {
                    id: id
                },
                method: 'GET',
                cache: true,
                url: CONSTANTS.url + '/public/getOutletById'
            });
        };
        this.getProductSequence = function() {

            return $http({
                method: 'GET',
                cache: true,
                url: CONSTANTS.url + '/public/getProductSequence'
            });
        };
        this.getCategoryProductSequence = function(categoryId) {

            return $http({
                params: {
                    categoryId: categoryId
                },
                method: 'GET',
                cache: true,
                url: CONSTANTS.url + '/public/getCategoryProductSequence'
            });
        };
        this.createVerificationCode = function() {
            return $http({
                params: {
                    mobileDeviceId: CONSTANTS.mobileAppId
                },
                method: 'POST',
                url: CONSTANTS.url + '/purchaseHelper/createVerificationCode'
            });
        };
        this.getProductSkus = function(productId) {
            return $http({
                params: {
                    productId: productId
                },
                method: 'GET',
                url: CONSTANTS.url + '/purchaseHelper/getProductSkus'
            });
        };
        this.getMobileApp = function(mobileAppId) {
            return $http({
                method: 'GET',
                params: {
                    mobileAppId: mobileAppId
                },
                cache: true,
                url: CONSTANTS.url + '/public/getMobileApp'
            });
        };
        this.addNewUserWithMobile = function(user, mobileAppId) {
            $log.info('user details', user);
            return $http({
                method: 'POST',
                params: {
                    mobileAppId: mobileAppId
                },
                data: {
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    mobile: user.mobile,
                    email: user.email
                },
                url: CONSTANTS.url + '/public/addNewUserWithMobile'
            });
        };
        this.getUserDetails = function(mobileAppId) {
            return $http({
                method: 'GET',
                params: {
                    mobileAppId: mobileAppId
                },
                url: CONSTANTS.url + '/public/getUserDetails'
            });
        };
        this.createMobileApp = function(mobileApp) {
            $log.info('mobileApp:', mobileApp);

            return $http({
                method: 'POST',
                url: CONSTANTS.url + '/public/createMobileApp',
                data: {
                    appId: mobileApp.appId,
                    cordova: mobileApp.cordova,
                    device: mobileApp.device,
                    platform: mobileApp.platform1,
                    model: mobileApp.model,
                    platformVersion: mobileApp.platformVersion

                }
            });
        };
        this.updateGcmIdForMobile = function(mobileApp) {
            return $http({
                method: 'POST',
                url: CONSTANTS.url + '/public/updateGcmIdForMobile',
                params: {
                    mobileAppId: mobileApp.appId,
                    gcmId: mobileApp.gcmId
                }
            });
        };

        this.attachUserToMobile = function(mobileAppId, userId) {
            return $http({
                method: 'POST',
                params: {
                    mobileAppId: mobileAppId,
                    userId: userId
                },
                url: CONSTANTS.url + '/public/attachUserToMobile'

            });
        };
    });
;// Source: app/js/services/pushNotifications.js
// ALL GCM notifications come through here. 
function onNotificationGCM(e) {
    // console.log('EVENT -&gt; RECEIVED:' + e.event + '');
    switch (e.event) {
        case 'registered':

            // if ( e.regid.length &gt; 0 )
            // {
            //     console.log('REGISTERED with GCM Server -&gt; REGID:' + e.regid + &quot;&quot;);

            //     //call back to web service in Angular.  
            //     //This works for me because in my code I have a factory called
            //      PushProcessingService with method registerID
            var elem = angular.element(document.querySelector('[ng-app]'));
            var injector = elem.injector();
            var publicservice = injector.get('Publicservice');
            var CONSTANTS = injector.get('CONSTANTS');
            //set gcmID here
            var mobileapp = {};
            CONSTANTS.gcmId = e.regid;
            mobileapp.gcmId = e.regid;
            mobileapp.appId = CONSTANTS.mobileAppId;
            publicservice.updateGcmIdForMobile(mobileapp).success(function(data) { }).error(function(data) {   });
            break;

        case 'message':
            // if this flag is set, this notification happened while we were in the foreground.
            // you might want to play a sound to get the user's attention, throw up a dialog, etc.
            if (e.foreground) {
                //we're using the app when a message is received.
                alert('--INLINE NOTIFICATION--' + '');

                // if the notification contains a soundname, play it.
                //var my_media = new Media(&quot;/android_asset/www/&quot;+e.soundname);
                //my_media.play();
                alert(e.payload.message);
            } else {
                // otherwise we were launched because the user touched a notification in the notification tray.
                if (e.coldstart) {
                    // Application is opened by msg
                } else {
                    //alert('--BACKGROUND NOTIFICATION--' + '');
                    // Application is running in background
                }

                // direct user here:
                // window.location = &quot;#/tab/featured&quot;;
            }

            var url = e.payload.message.show;
            //redirect to page
            window.location = url;
            break;

        case 'error':
            // console.log('ERROR -&gt; MSG:' + e.msg + '');
            alert(e.msg);
            break;

        default:
            alert('Unknown error');
            // console.log('EVENT -&gt; Unknown, an event was received and we do not know what it is');
            break;
    }
}
;// Source: app/js/services/user.js
angular.module('wairyum-service-mob')
    .service('UserService', function($log, CONSTANTS, $resource) {
        var Service = $resource(CONSTANTS.url + '/users/:id', {}, {
            query: {
                method: 'GET',
                params: {
                    offset: 0,
                    max: 10
                },
                isArray: false
            }
        });
        this.get = function(offset, max, name) {
            return Service.query({
                offset: offset,
                name: name,
                max: max
            }, function(users) {

                $log.info('User-Service : ', users);
                return users;
            });
        };
        this.save = function(user) {
            $log.debug('user details:', user);
            return Service.save({}, user, function(responce) {
                $log.debug(responce)
            }, function(responce) {
                $log.debug(responce)
            });
        };
    });
;// Source: app/js/services/wishlist.js
angular.module('wairyum-service-mob')
    .service('Wishlist', function($log, CONSTANTS, $resource, $http) {


        this.addWishItem = function(productId) {
            return $http({
                method: 'POST',
                url: CONSTANTS.url + '/purchaseHelper/addWishItem',
                data: {
                    productId: productId,
                    wishDate: ' 2014-09-24T19:30:23Z',
                    mobileAppId: CONSTANTS.mobileAppId
                }
            });
        };
        this.getWishItem = function(mobileAppId) {
            return $http({
                params: {
                    mobileAppId: mobileAppId
                },
                method: 'GET',

                url: CONSTANTS.url + '/purchaseHelper/getWishtItems'
            });
        };
        this.deleteOrderItem = function(wishItemId) {
            $log.debug('id of orderItem to be deleted', wishItemId);

            return $http({
                params: {
                    id:wishItemId
                },
                method: 'DELETE',
                url: CONSTANTS.url + '/purchaseHelper/deleteWishItem'

            });

        };



    });
