'use strict';
angular.module('wairyum-service-mob')
    .service('PurchaseHitoryHelper', function CategoryHelper($http, $q, $rootScope, $log, $resource, CONSTANTS) {

        this.getPurchaseHistory = function(mobileDeviceId) {
            return $http({
                params: {
                    mobileDeviceId: mobileDeviceId
                },
                method: 'GET',

                url: CONSTANTS.url + '/purchaseHelper/getPurchaseHistory'

            });
        };
    });
