'use strict';
angular.module('wairyum-service-mob')
.service('CategoryHelper', function CategoryHelper($http, $q, $rootScope, $log, $resource, CONSTANTS) {
    
    this.getCategoryByParentId = function(parentId) {
    return $http({
            params: {
                parentId: parentId
            },
            method: 'GET',
          
            url: CONSTANTS.url + '/categoryHelper/getCategoryByParentId'

        });
    };
    this.getProductsByCategoryId = function(id) {

        return $http({
            params: {
                id: id 
            },
            method: 'GET',
         
            url: CONSTANTS.url + '/categoryHelper/getProductsByCategoryId'

        });
    };
});
