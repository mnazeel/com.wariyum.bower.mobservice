'use strict';

angular.module('wairyum-service-mob')
    .service('UserService', function($log, CONSTANTS, $resource) {
        var Service = $resource(CONSTANTS.url + '/users/:id', {}, {
            query: {
                method: 'GET',
                params: {
                    offset: 0,
                    max: 10
                },
                isArray: false
            }
        });
        this.get = function(offset, max, name) {
            return Service.query({
                offset: offset,
                name: name,
                max: max
            }, function(users) {

                $log.info('User-Service : ', users);
                return users;
            });
        };
        this.save = function(user) {
            $log.debug('user details:', user);
            return Service.save({}, user, function(responce) {
                $log.debug(responce)
            }, function(responce) {
                $log.debug(responce)
            });
        };
    });
