'use strict';

angular.module('wairyum-service-mob')
    .service('Publicservice', function Publicservice($http, $q, $rootScope, $log, CONSTANTS, MobileAppService) {
        this.getProducts = function(offset, max) {

            return $http({
                method: 'GET',
                params: {
                    offset: offset,
                    max: max
                },
                cache: true,
                url: CONSTANTS.url + '/public/getProducts'
            });
        };
        this.getProductById = function(id) {
            return $http({
                params: {
                    id: id
                },
                method: 'GET',
                cache: true,
                url: CONSTANTS.url + '/public/getProductById'
            });
        };
        this.getMobileAppByAppId = function(appId) {
            return $http({
                params: {
                    appId: appId
                },
                method: 'GET',
                cache: true,
                url: CONSTANTS.url + '/public/getMobileAppByAppId'
            });
        };
        this.getOutlets = function() {
            return $http({
                method: 'GET',
                cache: true,
                url: CONSTANTS.url + '/public/getOutlets'

            });
        };
        this.getOutletsById = function(id) {

            return $http({
                params: {
                    id: id
                },
                method: 'GET',
                cache: true,
                url: CONSTANTS.url + '/public/getOutletById'
            });
        };
        this.getProductSequence = function() {

            return $http({
                method: 'GET',
                cache: true,
                url: CONSTANTS.url + '/public/getProductSequence'
            });
        };
        this.getCategoryProductSequence = function(categoryId) {

            return $http({
                params: {
                    categoryId: categoryId
                },
                method: 'GET',
                cache: true,
                url: CONSTANTS.url + '/public/getCategoryProductSequence'
            });
        };
        this.createVerificationCode = function() {
            return $http({
                params: {
                    mobileDeviceId: CONSTANTS.mobileAppId
                },
                method: 'POST',
                url: CONSTANTS.url + '/purchaseHelper/createVerificationCode'
            });
        };
        this.getProductSkus = function(productId) {
            return $http({
                params: {
                    productId: productId
                },
                method: 'GET',
                url: CONSTANTS.url + '/purchaseHelper/getProductSkus'
            });
        };
        this.getMobileApp = function(mobileAppId) {
            return $http({
                method: 'GET',
                params: {
                    mobileAppId: mobileAppId
                },
                cache: true,
                url: CONSTANTS.url + '/public/getMobileApp'
            });
        };
        this.addNewUserWithMobile = function(user, mobileAppId) {
            $log.info('user details', user);
            return $http({
                method: 'POST',
                params: {
                    mobileAppId: mobileAppId
                },
                data: {
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    mobile: user.mobile,
                    email: user.email
                },
                url: CONSTANTS.url + '/public/addNewUserWithMobile'
            });
        };
        this.getUserDetails = function(mobileAppId) {
            return $http({
                method: 'GET',
                params: {
                    mobileAppId: mobileAppId
                },
                url: CONSTANTS.url + '/public/getUserDetails'
            });
        };
        this.createMobileApp = function(mobileApp) {
            $log.info('mobileApp:', mobileApp);

            return $http({
                method: 'POST',
                url: CONSTANTS.url + '/public/createMobileApp',
                data: {
                    appId: mobileApp.appId,
                    cordova: mobileApp.cordova,
                    device: mobileApp.device,
                    platform: mobileApp.platform1,
                    model: mobileApp.model,
                    platformVersion: mobileApp.platformVersion

                }
            });
        };
        this.updateGcmIdForMobile = function(mobileApp) {
            return $http({
                method: 'POST',
                url: CONSTANTS.url + '/public/updateGcmIdForMobile',
                params: {
                    mobileAppId: mobileApp.appId,
                    gcmId: mobileApp.gcmId
                }
            });
        };

        this.attachUserToMobile = function(mobileAppId, userId) {
            return $http({
                method: 'POST',
                params: {
                    mobileAppId: mobileAppId,
                    userId: userId
                },
                url: CONSTANTS.url + '/public/attachUserToMobile'

            });
        };
    });
