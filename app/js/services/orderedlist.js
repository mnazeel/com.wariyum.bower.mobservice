'use strict';

angular.module('wairyum-service-mob')
    .service('Orderedlist', function($log, CONSTANTS, $resource, $http) {
        this.getPurchaseOrders= function(purchaseId) {
            return $http({
                params: {
                    purchaseId:purchaseId
                },
                method: 'GET',
                url: CONSTANTS.url + '/purchaseHelper/getPurchaseOrders'

            });
        };
        this.getOrderedItems = function(mobileAppId) {
            return $http({
                params: {
                    mobileDeviceId: mobileAppId
                },
                method: 'GET',
                url: CONSTANTS.url + '/purchaseHelper/getOrderedItems'

            });
        };
        this.getOrderDetail = function(skuId) {
            return $http({
                params: {
                    mobileAppId: CONSTANTS.mobileAppId,
                    skuId: skuId
                },
                method: 'POST',
                url: CONSTANTS.url + '/purchaseHelper/getOrderDetail'

            });
        };
        this.saveorderitem = function(orderitem) {
            $log.debug('orderitem before save:', orderitem);
            return $http({
                params: {
                    mobileAppId: CONSTANTS.mobileAppId
                },
                method: 'POST',
                url: CONSTANTS.url + '/purchaseHelper/saveOrderItem',
                data: {
                    skuId: orderitem.skuId,
                    //id: orderitem.id,
                    quantity: orderitem.quantity
                }
            });
        };
        this.deleteOrderItem = function(orderitemId) {
            $log.debug('id of orderItem to be deleted', orderitemId);

            return $http({
                params: {
                    id: orderitemId
                },
                method: 'POST',
                url: CONSTANTS.url + '/purchaseHelper/deleteOrderItem'

            });
        };
        this.convertOrderToPurchase = function(mobileAppId) {
            return $http({
                params: {
                    mobileAppId: mobileAppId
                },
                method: 'POST',
                url: CONSTANTS.url + '/purchaseHelper/convertOrderToPurchase'

            });
        };


    });
