'use strict';

angular.module('wairyum-service-mob')
    .service('SearchHelper', function SearchHelper($http, $q, $rootScope, $log, CONSTANTS) {
        this.getSearchResult = function(mobileDeviceId) {
          return $http({
                params: {
                    mobileDeviceId: mobileDeviceId
                },
                method: 'POST',
                cache: true,
                url: CONSTANTS.url + '/SearchHelper/getSearchResult'

            });
        };

        this.setSearch = function(searchCondition) {
            $log.debug('searchConditions:',searchCondition);
            return $http({
                method: 'POST',
                url: CONSTANTS.url + '/SearchHelper/setSearch',
                data: {
                    isOnCategory:searchCondition.category,
                    isOnProduct:searchCondition.product,
                    searchText:searchCondition.searchtext,
                    mobileDeviceId:CONSTANTS.mobileAppId
                }
            });
        };


    });
