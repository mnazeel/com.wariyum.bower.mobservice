'use strict';

// ALL GCM notifications come through here. 
function onNotificationGCM(e) {
    // console.log('EVENT -&gt; RECEIVED:' + e.event + '');
    switch (e.event) {
        case 'registered':

            // if ( e.regid.length &gt; 0 )
            // {
            //     console.log('REGISTERED with GCM Server -&gt; REGID:' + e.regid + &quot;&quot;);

            //     //call back to web service in Angular.  
            //     //This works for me because in my code I have a factory called
            //      PushProcessingService with method registerID
            var elem = angular.element(document.querySelector('[ng-app]'));
            var injector = elem.injector();
            var publicservice = injector.get('Publicservice');
            var CONSTANTS = injector.get('CONSTANTS');
            //set gcmID here
            var mobileapp = {};
            CONSTANTS.gcmId = e.regid;
            mobileapp.gcmId = e.regid;
            mobileapp.appId = CONSTANTS.mobileAppId;
            publicservice.updateGcmIdForMobile(mobileapp).success(function(data) { }).error(function(data) {   });
            break;

        case 'message':
            // if this flag is set, this notification happened while we were in the foreground.
            // you might want to play a sound to get the user's attention, throw up a dialog, etc.
            if (e.foreground) {
                //we're using the app when a message is received.
                alert('--INLINE NOTIFICATION--' + '');

                // if the notification contains a soundname, play it.
                //var my_media = new Media(&quot;/android_asset/www/&quot;+e.soundname);
                //my_media.play();
                alert(e.payload.message);
            } else {
                // otherwise we were launched because the user touched a notification in the notification tray.
                if (e.coldstart) {
                    // Application is opened by msg
                } else {
                    //alert('--BACKGROUND NOTIFICATION--' + '');
                    // Application is running in background
                }

                // direct user here:
                // window.location = &quot;#/tab/featured&quot;;
            }

            var url = e.payload.message.show;
            //redirect to page
            window.location = url;
            break;

        case 'error':
            // console.log('ERROR -&gt; MSG:' + e.msg + '');
            alert(e.msg);
            break;

        default:
            alert('Unknown error');
            // console.log('EVENT -&gt; Unknown, an event was received and we do not know what it is');
            break;
    }
}
