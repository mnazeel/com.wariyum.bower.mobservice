'use strict';

angular.module('wairyum-service-mob')
    .service('Wishlist', function($log, CONSTANTS, $resource, $http) {


        this.addWishItem = function(productId) {
            return $http({
                method: 'POST',
                url: CONSTANTS.url + '/purchaseHelper/addWishItem',
                data: {
                    productId: productId,
                    wishDate: ' 2014-09-24T19:30:23Z',
                    mobileAppId: CONSTANTS.mobileAppId
                }
            });
        };
        this.getWishItem = function(mobileAppId) {
            return $http({
                params: {
                    mobileAppId: mobileAppId
                },
                method: 'GET',

                url: CONSTANTS.url + '/purchaseHelper/getWishtItems'
            });
        };
        this.deleteOrderItem = function(wishItemId) {
            $log.debug('id of orderItem to be deleted', wishItemId);

            return $http({
                params: {
                    id:wishItemId
                },
                method: 'DELETE',
                url: CONSTANTS.url + '/purchaseHelper/deleteWishItem'

            });

        };



    });
