'use strict';

angular.module('wairyum-service-mob')
    .service('MobileAppService', function($log, CONSTANTS, $resource) {
        var Service = $resource(CONSTANTS.url + '/mobileApps/:id', {}, {
            query: {
                method: 'GET',
                params: {
                    offset: 0,
                    max: 10
                },
                isArray: true
                // headers: {
                //     'X-Auth-Token': $rootScope.authHeader
                // }

            }
        });
        this.get = function(offset, max, name) {
            // if ($rootScope.isAuthenticated) {
            return Service.query({
                offset: offset,
                name: name,
                max: max
            }, function(users) {

                // loaded = loaded + 3;
                //todo: limit to maximum products
                return users;
            });
            //};
        };
        this.save = function(mobileapp) {
            alert('in MobileAppService');
            alert(mobileapp.gcmId)
            return Service.save({}, mobileapp, function(response) {
                // Handle a successful response
                alert(response);
            }, function(response) {
                alert('error');
                // Handle a non-successful response
            });
        };
    });
